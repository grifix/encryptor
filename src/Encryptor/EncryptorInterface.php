<?php

declare(strict_types=1);

namespace Grifix\Encryptor\Encryptor;

use Grifix\Encryptor\Encryptor\Exceptions\CannotDecryptException;

interface EncryptorInterface
{
    public function encrypt(string $value): string;

    /**
     * @throws CannotDecryptException
     */
    public function decrypt(string $value): string;
}
