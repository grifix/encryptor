<?php

declare(strict_types=1);

namespace Grifix\Encryptor\KeyGenerator;

interface KeyGeneratorInterface
{
    public function generateKey(): string;
}
