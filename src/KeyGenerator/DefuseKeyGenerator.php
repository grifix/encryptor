<?php

declare(strict_types=1);

namespace Grifix\Encryptor\KeyGenerator;

use Defuse\Crypto\Key;

final class DefuseKeyGenerator implements KeyGeneratorInterface
{
    public function generateKey(): string
    {
        return Key::createNewRandomKey()->saveToAsciiSafeString();
    }
}
