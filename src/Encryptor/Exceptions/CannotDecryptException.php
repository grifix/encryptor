<?php

declare(strict_types=1);

namespace Grifix\Encryptor\Encryptor\Exceptions;

use Throwable;

final class CannotDecryptException extends \Exception
{
    public function __construct(Throwable $previous)
    {
        parent::__construct('Cannot decrypt data!', previous: $previous);
    }
}
