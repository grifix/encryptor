<?php

declare(strict_types=1);

namespace Grifix\Encryptor\Encryptor;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Grifix\Encryptor\Encryptor\Exceptions\CannotDecryptException;

final class DefuseEncryptor implements EncryptorInterface
{
    private readonly Key $key;

    public function __construct(string $key)
    {
        $this->key = Key::loadFromAsciiSafeString($key);
    }

    public function encrypt(string $value): string
    {
        return Crypto::encrypt($value, $this->key);
    }

    /**
     * @inheritdoc
     */
    public function decrypt(string $value): string
    {
        try {
            return Crypto::decrypt($value, $this->key);
        } catch (\Throwable $exception) {
            throw new CannotDecryptException($exception);
        }
    }
}
