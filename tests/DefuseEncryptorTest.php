<?php

declare(strict_types=1);

namespace Grifix\Encryptor\Tests;

use Grifix\Encryptor\Encryptor\DefuseEncryptor;
use Grifix\Encryptor\Encryptor\Exceptions\CannotDecryptException;
use Grifix\Encryptor\KeyGenerator\DefuseKeyGenerator;
use PHPUnit\Framework\TestCase;

final class DefuseEncryptorTest extends TestCase
{
    public function testItWorks(): void
    {
        $encryptor = new DefuseEncryptor($this->generateKey());
        $string = 'secret';
        $encryptedString = $encryptor->encrypt($string);
        self::assertEquals($string, $encryptor->decrypt($encryptedString));
    }

    public function testItFailsToDecrypt(): void
    {
        $encryptorA = new DefuseEncryptor($this->generateKey());
        $encryptorB = new DefuseEncryptor($this->generateKey());

        $data = $encryptorA->encrypt('secret');
        $this->expectException(CannotDecryptException::class);
        $encryptorB->decrypt($data);
    }

    private function generateKey(): string
    {
        return (new DefuseKeyGenerator())->generateKey();
    }
}
